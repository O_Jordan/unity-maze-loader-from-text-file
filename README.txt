This project loads a maze from a text file, including targets to shoot, linked doors and a goal.

You have a set amount of ammo to complete the maze. Shoot targets to open doors.

CONTROLS
WASD - Move
Mouse - Look
Left Click - shoot
R - reset level
ESC - quit