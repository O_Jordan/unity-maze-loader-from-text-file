﻿using UnityEngine;

[System.Serializable]
public class TileTextureMaterial {
    public string Name;
    public Texture2D Texture;
    public TileType Tiletype;
}
