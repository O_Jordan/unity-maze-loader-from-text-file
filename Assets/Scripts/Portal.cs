﻿using UnityEngine;

public class Portal : MonoBehaviour {
    public int ToRoom { get; set; }
    public bool JustArrived { get; set; }

    private void OnTriggerEnter(Collider other)
    {
        if(JustArrived)
        {
            //exit and return early
            return;
        }
        //check if it was the player game object that entered the portal
        //player entered portal, broadcast to any method listening the room number the player entered

        if(other.gameObject.tag == "Player")
        {
            Messenger<int>.Broadcast(GameEvent.ENTERED_PORTAL, ToRoom);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            JustArrived = false;
        }
    }
}
