﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletControl : MonoBehaviour {

    private void Awake()
    {
        //Add listener for the BULLET_HIT event
        Messenger<Collider>.AddListener(GameEvent.BULLET_HIT, DeleteBullet);
    }
    // Use this for initialization
    void Start () {
		
	}

    private void DeleteBullet(Collider bullet)
    {
        //destroy the gameobject connected to the provided collider
        GameObject.Destroy(bullet.gameObject);
    }
}
