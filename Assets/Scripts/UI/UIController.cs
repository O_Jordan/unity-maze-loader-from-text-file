﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIController : MonoBehaviour {
    //create private fields, configurable in the Unity Editor
    [SerializeField]
    private Texture2D frameTexture;
    [SerializeField]
    private Texture2D playerTexture;
    [SerializeField]
    private Texture2D healthPlayerTexture;
    [SerializeField]
    private GUISkin customSkin;
    [SerializeField]
    private Texture2D crosshair;

    //varibale to hold the player's weapon model
    GameObject weapon;
    //variable to hold the script attached to said model
    FPSWeapon weaponScript;
    //variables to hold the time when started and time passed
    private DateTime startTime;
    private TimeSpan elapsedTime;

    //variables holding current ammo count, max possible ammo count
    float ammo;
    float maxAmmo;

    // Use this for initialization
    void Start () {
        //set the startTime to now
        startTime = DateTime.Now;
        //set the elapsed Time to 0
        elapsedTime = TimeSpan.Zero;
        //find and store the player's weapon in weapon
        weapon = GameObject.FindWithTag("weapon");
        //get the script attacthed to the player's weapon
        weaponScript = weapon.GetComponent<FPSWeapon>();
	}
	
	// Update is called once per frame
	void Update () { 
        //update the elapsed time
        elapsedTime = DateTime.Now - startTime;
	}

    private void OnGUI()
    {
        //set GUI sizes and common positions
        int width = 200;
        int height = 25;
        int margin = 10;
        int leftX = margin;
        int rightX = Screen.width - width - margin;
        int posY = 5;
        int labelWidth = 80;
        int healthBarWidth = 156;
        int healthBarHeight = 21;
        //get the ammo and max ammo from the weapon
        ammo = weaponScript.Ammo;
        maxAmmo = weaponScript.MaxAmmo;
        //set the GUI's skin
        GUI.skin = customSkin;
        //draw GUI elements
        GUI.Box(new Rect(rightX, posY, width, height), "");
        GUI.Label(new Rect(rightX + margin, posY, labelWidth, height), "TIME: ");
        GUI.Label(new Rect(rightX + margin + labelWidth, posY, (width - labelWidth), height), string.Format("{0:00}:{1:00}:{2:00}", elapsedTime.Hours, elapsedTime.Minutes, elapsedTime.Seconds));
        GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, 30, 30),crosshair);
        posY *= 2;
        //only show this if player has no ammo left
        if (ammo<=0)
        {
            GUI.Label(new Rect(rightX, posY + height, width, height), "Out of ammo!");
        }
        

        labelWidth = 46;

        GUI.DrawTexture(new Rect(leftX, posY, labelWidth, 32), playerTexture);

        posY = 15;

        using (new GUI.GroupScope(new Rect(leftX + labelWidth + margin, posY, healthBarWidth, height)))
        {
            GUI.DrawTexture(new Rect(0, 0, healthBarWidth, healthBarHeight), frameTexture);

            //health bar total depends on ammo used, compared to max ammo
            using (new GUI.GroupScope(new Rect(0, 0, (ammo / maxAmmo) * healthBarWidth, healthBarHeight)))
            {
                GUI.DrawTexture(new Rect(0, 0, healthBarWidth, healthBarHeight), healthPlayerTexture);
            }
        }

        Cursor.lockState = CursorLockMode.Locked;
    }
}
