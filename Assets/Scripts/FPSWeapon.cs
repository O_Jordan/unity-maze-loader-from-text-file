﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class FPSWeapon : MonoBehaviour {
    AudioSource gunaudio;
    AudioSource noAmmo;
    public float recoil;
    public GameObject bulletPrefab;
    public GameObject Player;
    public GameObject Camera;
    private GameObject parentTileAnchor;
    public int Ammo;
    public int MaxAmmo;
    private void Awake()
    {
        parentTileAnchor = this.gameObject;
    }
    void Start () {
        //get the audio components
        gunaudio = GetComponent<AudioSource>();
        noAmmo = transform.parent.GetComponent<AudioSource>();
        //initialise the recoil value
        recoil = 0.0f;
	}

    // Update is called once per frame
    void Update()
    {
        float yrotate = Player.transform.rotation.y;
        float xrotate = Camera.transform.rotation.x;
        bool fire = Input.GetButtonDown("Fire1");
        
        if (fire == true && Ammo > 0)
        {
            //only runs if there's ammo left and the fire button was pressed down (doesn't run more than once if fire is held down)
            //play the firing audio
            gunaudio.Play();
            //create a new bulet from the bullet prefab
            GameObject newbullet = GameObject.Instantiate(bulletPrefab);
            //get the bullet script from the new bullet
            Bullet thebullet = newbullet.GetComponent<Bullet>();
            //set the parent to be the weapon
            thebullet.transform.parent = parentTileAnchor.transform;
            //place the bullet relative to the parent (in this case at the gun's barrel)
            thebullet.transform.localPosition = new Vector3(0.0f, 0.107f, -0.821f);
            //rotate the bullet appropriately
            thebullet.transform.localRotation = Quaternion.Euler(thebullet.transform.localRotation.x + xrotate + 90, thebullet.transform.localRotation.y + yrotate, 0);
            //unattacth the bullet from the weapon (it gets a new parent in the bullet script)
            thebullet.transform.parent = null;
            //reduce ammo count by one
            Ammo = Ammo - 1;
            if (recoil < 50)
            {
                //rotate the weapon to create simple script controlled recoil animation, depending on recoil level
                transform.Rotate(new Vector3(transform.localRotation.x + 10, 0, 0));
                //increase the recoil
                recoil = recoil + 10;
            }
        }
        else if (fire == true && Ammo <= 0)
        {
            //no ammo available, play noammo audio
            noAmmo.Play();
        }

        if (recoil > 0)
        {
            //everyframe, if recoil is still in effect, move the weapon back towards resting position and reduce the recoil level
            transform.Rotate(new Vector3(transform.localRotation.x - 1, 0, 0));
            recoil = recoil - 1;
        }
    }

}
