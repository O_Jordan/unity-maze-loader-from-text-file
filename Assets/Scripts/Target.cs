﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {
    public int targetCode;
	// Use this for initialization
	void Start () {
        //add listener for TARGET_HIT event
        Messenger<int>.AddListener(GameEvent.TARGET_HIT, openLinkedLock);
        SetGameObjectName();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void openLinkedLock(int thisTargetCode)
    {
        //set target lock
        string doorTarget = string.Format("DoorPrefab_Code_{0}", thisTargetCode);
        //fopr every child in the room
        foreach (Transform child in GameObject.Find("Room").transform)
        {
            //if the object is a lock
            if (child.tag.ToLower() == "lock")
            {
                //if the lock is the target lock
                if (child.name == doorTarget)
                {
                    //get the lock script
                    Lock unlockThis = child.GetComponent<Lock>();
                    //set it to unlocked and opening
                    unlockThis.opening = true;
                }
                
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if a bullet collides with the target, broadcast the TARGET_HIT event
        if (other.tag.ToLower() == "bullet")
        {
            Messenger<int>.Broadcast(GameEvent.TARGET_HIT, targetCode);
        }
    }

    private void OnDestroy()
    {
        //remove listeners
        Messenger<int>.RemoveListener(GameEvent.TARGET_HIT, openLinkedLock);
    }

    private void SetGameObjectName()
    {
        //Check if texture has a name
        //if yes, set the game object created from this prefab
        if (targetCode >= 0)
        {
            name = string.Format("TargetPrefab_Code_{0}", targetCode);
        }
    }
}
