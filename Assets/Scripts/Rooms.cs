﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("rooms")]
public class Rooms {
    [XmlElement("room")]
    public List<Room> Collection { get; set; }
	
}
