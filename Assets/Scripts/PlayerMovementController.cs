﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovementController : MonoBehaviour {


    public float speed = 6.0f;
    public float gravity = -9.8f;
    private CharacterController _charController;
    private Rigidbody body;
    private Animator anim;
    private int hFloat;                            // Animator variable related to Horizontal Axis.
    private int vFloat;
    private bool isLightOn;
    Light playerLight;
    // Use this for initialization
    void Start () {
		_charController = GetComponent<CharacterController>();
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        hFloat = Animator.StringToHash("H");
        vFloat = Animator.StringToHash("V");
        isLightOn = true;
        playerLight = GameObject.FindWithTag("playerlight").GetComponent<Light>();
    }
    // Update is called once per frame
    void Update () {

    

        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;
        Vector3 movement = new Vector3(deltaX, 0, deltaZ);
        movement = Vector3.ClampMagnitude(movement, speed);
        movement.y = gravity;
        movement *= Time.deltaTime;

        movement = transform.TransformDirection(movement);
        _charController.Move(movement);
        //broadcast events related to various button presses
        if (Input.GetButtonDown("Fire2"))
        {
            Messenger.Broadcast(GameEvent.RESTART_GAME);
        }

        if(Input.GetButtonDown("Fire3"))
        {
            Messenger.Broadcast(GameEvent.RESTART_LEVEL);
        }

        if(Input.GetButton("Cancel"))
        {
            Application.Quit();
        }
        //torch control
        if(Input.GetButtonDown("Jump"))
        {
            //if on, turn it off
            if (isLightOn)
            {
                playerLight.intensity = 0;
                isLightOn = false;
            }
            //if off, turn it on
            else
            {
                playerLight.intensity = 1;
                isLightOn = true;
            }
            
        }
    }
}
