﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonMouseLook : MonoBehaviour {

	public enum RotationAxes
    {
        MouseXAndY = 0,
        MouseX = 1,
        MouseY = 2
    }

    public RotationAxes axes = RotationAxes.MouseXAndY;

    public float horizontalspeed = 9.0f;
    public float verticalspeed = 9.0f;
    public float minVerticalRotation = -45.0f;
    public float maxVerticalRotation = 45.0f;

    private float _rotationX = 0;
    
    // Use this for initialization
	void Start () {
        Rigidbody body = GetComponent<Rigidbody>();

        if (body != null)
        {
            body.freezeRotation = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
        if (axes==RotationAxes.MouseX)
        {
            //code for horizontal rotation
            transform.Rotate(0, Input.GetAxis("Mouse X") * horizontalspeed, 0);
        }
        else if (axes == RotationAxes.MouseY)
        {
            //code for vertical rotation
            _rotationX -= Input.GetAxis("Mouse Y") * verticalspeed;
            _rotationX = Mathf.Clamp(_rotationX, minVerticalRotation, maxVerticalRotation);
            float rotationY = transform.localEulerAngles.y;
            transform.localEulerAngles = new Vector3(_rotationX, rotationY, 0);

        }
        else
        {
            //code for diagonal rotation
            _rotationX -= Input.GetAxis("Mouse Y") * verticalspeed;
            _rotationX = Mathf.Clamp(_rotationX, minVerticalRotation, maxVerticalRotation);
            float deltaRotationX = Input.GetAxis("Mouse X") * horizontalspeed;
            float rotationY = transform.localEulerAngles.y + deltaRotationX;
            transform.localEulerAngles = new Vector3(_rotationX, rotationY, 0);
        }

	}
}
