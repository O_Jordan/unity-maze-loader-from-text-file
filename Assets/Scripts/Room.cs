﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

[XmlType("room")]
public class Room {
    [XmlAttribute("floor")]
    public string FloorMaterial { get; set;}
    [XmlAttribute("wall")]
    public string WallMaterial { get; set; }
    [XmlAttribute("door")]
    public string DoorMaterial { get; set; }
    [XmlAttribute("num")]
    public int Number { get; set; }
    [XmlAttribute("length")]
    public int Length { get; set; }
    [XmlAttribute("width")]
    public int Width { get; set; }
    [XmlAttribute("floors")]
    public int Floors { get; set; }
    [XmlAttribute("ammo")]
    public int MaxAmmo { get; set; }
    private string _Layout;
    [XmlText()]
    public string Layout
    {
        get { return _Layout; }
        set
        {
            _Layout = value.Trim();
        }
    }
}
