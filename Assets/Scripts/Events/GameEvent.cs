﻿

public class GameEvent
{
    //add a new event for when the player character enters a portal
    public const string ENTERED_PORTAL = "ENTERED_PORTAL";
    //add a new event for detecting when a bullet has hit something, used to tell bullet to delete itself
    public const string BULLET_HIT = "BULLET_HIT";
    //add a new event, used for targets to detect when they've been shot, and to unlock their corresponding lock
    public const string TARGET_HIT = "TARGET_HIT";
    //add a new event, used when a key is picked up, activates the level's portal 
    public const string KEY_COLLECTED = "KEY_COLLECTED";
    //add a new event, used when the key to restart a game is pressed, reloads the scene
    public const string RESTART_GAME = "RESTART_GAME";
    //add a new event, similar to above, but instead of reloading the scene, just rebuild the current room
    public const string RESTART_LEVEL = "RESTART_LEVEL";
}
