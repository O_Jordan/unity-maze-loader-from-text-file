﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class door : MonoBehaviour {

    public float Height { get; set; }

    public TileTextureMaterial _TextureMaterial;
    public TileTextureMaterial TextureMaterial
    {
        get
        {
            return _TextureMaterial;
        }
        set
        {
            _TextureMaterial = value;
            if (value != null)
            {
                SetGameObjectName();
                SetMaterial();
            }
        }
    }

    private void SetGameObjectName()
    {
        //Check if texture has a name
        //if yes, set the game object created from this prefab
        if (TextureMaterial.Name != null)
        {
            name = string.Format("TilePrefab_{0}", TextureMaterial.Name);
        }
    }

    private void SetMaterial()
    {
        GetComponent<Renderer>().material.mainTexture = TextureMaterial.Texture;
    }

}
