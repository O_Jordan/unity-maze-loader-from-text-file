﻿public enum Key
{
    Space = 0,
    Door = 1,
    Wall = 2,
    Floor = 3,
    Enemy = 4,
    Player = 5,
    NewFloor = 6,
    TargetN = 7,
    TargetS = 8,
    TargetE = 9,
    TargetW = 10,
    Lock = 11,
    RampN = 12,
    RampS = 13,
    RampE = 14,
    RampW = 15,
    Key = 16
}
