﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : MonoBehaviour {

    public int lockCode;
    public bool opening = false;
    bool open = false;
	// Use this for initialization
	void Start () {
        SetGameObjectName();
	}
	
	// Update is called once per frame
	void Update () {
		if (opening && !open)
        {
            //shrink portal until it's small, then deactivate it
            this.transform.localScale = this.transform.localScale - new Vector3(0.01f, 0.01f, 0.01f);
            if (this.transform.localScale == Vector3.zero)
            {
                opening = false;
                open = true;
                this.gameObject.SetActive(false);
            }
        }
	}

    private void SetGameObjectName()
    {
        //Check if texture has a name
        //if yes, set the game object created from this prefab
        if (lockCode >= 0)
        {
            name = string.Format("DoorPrefab_Code_{0}", lockCode);
        }
    }
}
