﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.IO;
using UnityEngine.SceneManagement;

public class LayoutControl : MonoBehaviour {


    

    public GameObject Player;
    public TextAsset RoomsLayoutXml;
    public TileTextureMaterial[] TileTextures;
    public GameObject TilePrefab;
    public GameObject TargetWall;
    public GameObject PortalPrefab;
    public GameObject DoorPrefab;
    public GameObject RampPrefab;
    public GameObject keyPrefab;
    public int WallHeight = 1;
    public int RoomNumber = 0;
    private GameObject parentTileAnchor;
    private Rooms rooms;
    private Room currentRoom;
    private bool isFirstRoom;
    private List<Portal> currentRoomPortals;
    private int newFloorOffset = 0;
    private int newFloorHeight = 0;
    public int targetCode;
    public int lockCode;
    public bool isPortalActive;
	// Use this for initialization
	void Start ()
    {
        //build the first room when this script starts
        BuildRoom(RoomNumber);
	}

    private void Awake()
    {

        parentTileAnchor = this.gameObject;
        LoadRooms();
        isFirstRoom = true;
        
        //add listeners for ENTERED_PORTAL event, KEY_COLLECTED event, RESTART_GAME event and RESTART_LEVEL event
        Messenger<int>.AddListener(GameEvent.ENTERED_PORTAL, OnRoomChanged);
        Messenger.AddListener(GameEvent.KEY_COLLECTED, ActivatePortal);
        Messenger.AddListener(GameEvent.RESTART_GAME, RestartScene);
        Messenger.AddListener(GameEvent.RESTART_LEVEL, RestartLevel);
    }

    private void LoadRooms()
    {
        //define a serializer ready to deserialise XML data
        XmlSerializer ser = new XmlSerializer(typeof(Rooms));
        //Read the XML text as a memory stream
        MemoryStream ms =
            new MemoryStream(Encoding.UTF8.GetBytes(RoomsLayoutXml.text));
        //read the XML loaded into the memory stream quickly using an XML reader object
        XmlReader reader = XmlReader.Create(ms);
        //deserialise the read Xml - it returns a generic object
        //use casting to convert to the correct object type
        rooms = (Rooms)ser.Deserialize(reader);
        reader.Close();
        ms.Close();

    }

    private void DestroyLayoutTiles()
    {
        //loop through all child objects attached to the parent tile anchor (room game object)
        //if the found child does not have its tag set to floor, destroy it
        foreach (Transform child in parentTileAnchor.transform)
        {
            if (child.tag.ToLower() != "floor" && child.tag.ToLower() != "wall")
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        //destroy all bullets in bullet holder
        Transform bulletHolder = GameObject.Find("bulletHolder").transform;
        foreach (Transform child in bulletHolder)
        {
            GameObject.Destroy(child.gameObject);
        }
        
    }

    private void ResizeRoom()
    {
        //resize room and cautionary boundry walls
        GameObject.FindGameObjectWithTag("Floor").transform.localScale = new Vector3(currentRoom.Width +1, 1, currentRoom.Length+1);
        GameObject.Find("Wall1").transform.localScale = new Vector3(currentRoom.Width + 1, currentRoom.Floors + 1, 1);
        GameObject.Find("Wall2").transform.localScale = new Vector3(currentRoom.Width + 1, currentRoom.Floors + 1, 1);
        GameObject.Find("Wall3").transform.localScale = new Vector3(1, currentRoom.Floors + 1, currentRoom.Length + 1);
        GameObject.Find("Wall4").transform.localScale = new Vector3(1, currentRoom.Floors + 1, currentRoom.Length + 1);
        GameObject.Find("Wall1").transform.position = new Vector3(0, (currentRoom.Floors/2), (currentRoom.Length / 2) + 0.5f);
        GameObject.Find("Wall2").transform.position = new Vector3(0, (currentRoom.Floors / 2), (-currentRoom.Length / 2) - 0.5f);
        GameObject.Find("Wall3").transform.position = new Vector3((currentRoom.Width / 2) + 0.5f, (currentRoom.Floors / 2), 0);
        GameObject.Find("Wall4").transform.position = new Vector3((-currentRoom.Width / 2) - 0.5f, (currentRoom.Floors / 2), 0);
    }

    private Key GetMapCodeType(char mapCode)
    {
        //return relevant key code for passed in character, codes for locks and targets have their own numerical code assigned to each of them, set that
        switch (mapCode)
        {
            case 'X':
                return Key.Player;
            case ' ':
            case '_':
                return Key.Space;
            case '|':
                return Key.Wall;
            case '.':
                return Key.Floor;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                return Key.Door;
            case 'e':
                return Key.Enemy;
            case 'f':
                return Key.NewFloor;
            case 'A':
                targetCode = 0;
                return Key.TargetN;
            case 'B':
                targetCode = 1;
                return Key.TargetN;
            case 'C':
                targetCode = 2;
                return Key.TargetN;
            case 'D':
                targetCode = 3;
                return Key.TargetN;
            case 'G':
                targetCode = 4;
                return Key.TargetN;
            case 'H':
                targetCode = 5;
                return Key.TargetS;
            case 'I':
                targetCode = 6;
                return Key.TargetS;
            case 'J':
                targetCode = 7;
                return Key.TargetS;
            case 'K':
                targetCode = 8;
                return Key.TargetS;
            case 'L':
                targetCode = 9;
                return Key.TargetS;
            case 'M':
                targetCode = 10;
                return Key.TargetE;
            case 'N':
                targetCode = 11;
                return Key.TargetE;
            case 'O':
                targetCode = 12;
                return Key.TargetE;
            case 'P':
                targetCode = 13;
                return Key.TargetE;
            case 'Q':
                targetCode = 14;
                return Key.TargetE;
            case 'R':
                targetCode = 15;
                return Key.TargetW;
            case 'S':
                targetCode = 16;
                return Key.TargetW;
            case 'T':
                targetCode = 17;
                return Key.TargetW;
            case 'U':
                targetCode = 18;
                return Key.TargetW;
            case 'V':
                targetCode = 19;
                return Key.TargetW;
            case 'a':
                lockCode = 0;
                return Key.Lock;
            case 'b':
                lockCode = 1;
                return Key.Lock;
            case 'c':
                lockCode = 2;
                return Key.Lock;
            case 'd':
                lockCode = 3;
                return Key.Lock;
            case 'g':
                lockCode = 4;
                return Key.Lock;
            case 'h':
                lockCode = 5;
                return Key.Lock;
            case 'i':
                lockCode = 6;
                return Key.Lock;
            case 'j':
                lockCode = 7;
                return Key.Lock;
            case 'k':
                lockCode = 8;
                return Key.Lock;
            case 'l':
                lockCode = 9;
                return Key.Lock;
            case 'm':
                lockCode = 10;
                return Key.Lock;
            case 'n':
                lockCode = 11;
                return Key.Lock;
            case 'o':
                lockCode = 12;
                return Key.Lock;
            case 'p':
                lockCode = 13;
                return Key.Lock;
            case 'q':
                lockCode = 14;
                return Key.Lock;
            case 'r':
                lockCode = 15;
                return Key.Lock;
            case 's':
                lockCode = 16;
                return Key.Lock;
            case 't':
                lockCode = 17;
                return Key.Lock;
            case 'u':
                lockCode = 18;
                return Key.Lock;
            case 'v':
                lockCode = 19;
                return Key.Lock;
            case '#':
                return Key.RampS;
            case '~':
                return Key.RampN;
            case '!':
                return Key.RampW;
            case '/':
                return Key.RampE;
            case '+':
                return Key.Key;
            default:
                return Key.Space;
        }
    }

    private TileTextureMaterial GetTileTextureMaterial(TileType currentTileType)
    {
        //returns material of the passed in tile
        string roomCurrentTileMaterial;
        if (currentTileType == TileType.floor)
        {
            roomCurrentTileMaterial = currentRoom.FloorMaterial;
        }
        else if (currentTileType == TileType.door)
        {
            roomCurrentTileMaterial = currentRoom.DoorMaterial;
        }
        else
        {
            roomCurrentTileMaterial = currentRoom.WallMaterial;
        }
        foreach (TileTextureMaterial tileTexture in TileTextures)
        {
            if (tileTexture.Tiletype == currentTileType)
            {
                if (tileTexture.Name == roomCurrentTileMaterial)
                {
                    return tileTexture;
                }
            }
        }
        return null;
    }

    private void SetTilePosition(Tile tile, float x, float y, float z, bool target)
    {
        //used to offset tile by half of it's scale
        float tileScaleHalf = 0.5f;
        //get the first positions
        float room_Tile1_PosX = currentRoom.Width / 2.0f;
        float room_Tile1_PosZ = -currentRoom.Length / 2.0f;
        //get positions of this tile
        float xPos = (room_Tile1_PosX - x) - tileScaleHalf;
        float zPos = (room_Tile1_PosZ + z) + tileScaleHalf;
        //set y position
        float yPos = y + tile.Height;
        //set the tile's parents (the target prefab has the tile as a child so set's the prefab's root's parent) 
        if (target)
        {
            tile.transform.parent.position = new Vector3(xPos, yPos, zPos);
        }
        else
        {
            tile.transform.position = new Vector3(xPos, yPos, zPos);
        }
        
    }

    private Tile CreateTile(Key tileCode, int colNum, float y, int rowNum)
    {
        GameObject tileGameObject;
        Tile tile = null;

        //create tile depending on passed in key code
        if (tileCode == Key.TargetN || tileCode == Key.TargetS || tileCode == Key.TargetE || tileCode == Key.TargetW)
        {
            //initiate from relevant prefab
            tileGameObject = GameObject.Instantiate(TargetWall);
            foreach (Transform child in tileGameObject.transform)
            {
                if (child.transform.name == "TilePrefab")
                {
                    //get the tile script from the correct child component
                    tile = child.GetComponent<Tile>();
                }
                foreach (Transform child2 in tile.transform)
                {
                    //set the correct child's target code to the correct code
                    Target thisTarget = child2.GetComponent<Target>();
                    thisTarget.targetCode = targetCode;
                }
            }
        }
        else if (tileCode == Key.RampN || tileCode == Key.RampS || tileCode == Key.RampE || tileCode == Key.RampW)
        {
            //initiate the correct prefab
            tileGameObject = GameObject.Instantiate(RampPrefab);
            //get the tile script from the prefab
            tile = tileGameObject.GetComponent<Tile>();
        }
        else if (tileCode == Key.Lock)
        {
            //initiate the correct prefab
            tileGameObject = GameObject.Instantiate(DoorPrefab);
            //get the scripts from the object
            tile = tileGameObject.GetComponent<Tile>();
            Lock thisLock = tileGameObject.GetComponent<Lock>();
            //set the tile's lock code
            thisLock.lockCode = lockCode;
        }
        else
        {
            //initiate the correct prefab
            tileGameObject = GameObject.Instantiate(TilePrefab);
            //get the tile script from the object
            tile = tileGameObject.GetComponent<Tile>();
        }

        //setting textures
        if (tileCode == Key.Wall)
        {
            tile.TextureMaterial = GetTileTextureMaterial(TileType.wall);
            //setting tile height
            tile.Height = 1.0f;
            y = y - 0.2f;
        }
        else if (tileCode == Key.Lock)
        {   
            tile.TextureMaterial = GetTileTextureMaterial(TileType.door);
            tile.Height = 1.0f;
            y = y - 0.2f;

            if(newFloorOffset == 0)
            {
                //if on the bottom floor, add a floor tile beneath the door
                AddTile('.', rowNum, colNum);
            }
        }
        else if (tileCode == Key.TargetN || tileCode == Key.TargetS || tileCode == Key.TargetE || tileCode == Key.TargetW)
        {
            foreach (Transform child in tileGameObject.transform)
            {
                //finding tile object in children...
                if (child.transform.name == "TilePrefab")
                {
                    //set the tile's textures
                    tile.TextureMaterial = GetTileTextureMaterial(TileType.wall);
                    tile.Height = 1.0f;
                    y = y - 0.2f;
                }
            }
            //Rotate targets to correct orientation
            if (tileCode == Key.TargetN)
            {
                tile.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else if (tileCode == Key.TargetE)
            {
                tile.transform.rotation = Quaternion.Euler(0, 270, 0);
            }
            else if (tileCode == Key.TargetW)
            {
                tile.transform.rotation = Quaternion.Euler(0, 90, 0);
            }
            else
            {
                tile.transform.rotation = Quaternion.Euler(0, 0, 0);
            }               
        }
        //rotate targets to correct orientation, add a floor til if on the bottom floor
        else if (tileCode == Key.RampN)
        {
            tileGameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
            if (newFloorOffset == 0)
            {
                AddTile('.', rowNum, colNum);
            }
        }
        else if (tileCode == Key.RampS)
        {
            tileGameObject.transform.rotation = Quaternion.Euler(0, 270, 0);
            if (newFloorOffset == 0)
            {
                AddTile('.', rowNum, colNum);
            }
        }
        else if (tileCode == Key.RampE)
        {
            tileGameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
            if (newFloorOffset == 0)
            {
                AddTile('.', rowNum, colNum);
            }
        }
        else if (tileCode == Key.RampW)
        {
            tileGameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
            if (newFloorOffset == 0)
            {
                AddTile('.', rowNum, colNum);
            }
        }
        else
        {
            //set floor texture and resize to floor sizes
            tile.TextureMaterial = GetTileTextureMaterial(TileType.floor);
            tile.Height = 0.2f;
            tile.transform.localScale = new Vector3(1.0f, tile.Height, 1.0f);
        }
        //setting positions and parents...
        if (tileCode == Key.TargetN || tileCode == Key.TargetS || tileCode == Key.TargetE || tileCode == Key.TargetW)
        {
            SetTilePosition(tile, colNum, y, rowNum, true);
            Transform thisTarget = tile.transform.parent;
            thisTarget.parent = parentTileAnchor.transform;
        }
        else if (tileCode == Key.RampN || tileCode == Key.RampS || tileCode == Key.RampE || tileCode == Key.RampW)
        {
            SetTilePosition(tile, colNum, y+0.2f, rowNum, false);
            tile.transform.parent = parentTileAnchor.transform;
        }
        else
        {
            SetTilePosition(tile, colNum, y, rowNum, false);
            tile.transform.parent = parentTileAnchor.transform;
        }
        return tile;
    }

    private void AddTile(char currentMapCode, int rowNum, int colNum)
    {
        //get the type of tile from passed in char
        Key currentTileMapCode = GetMapCodeType(currentMapCode);
        //depending on returned key...
        if (currentTileMapCode == Key.Space)
        {
            //do nothing for a space (I.E Air tile)
            return;
        }
        if (currentTileMapCode == Key.Wall)
        {
            //for however high walls on each floor should be, create a new wall tile
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }

        else if (currentTileMapCode == Key.NewFloor)
        {
            //if new floor character found, change the offset to emulate starting from row 1 in XML layout, add one to the new floor height
            newFloorOffset = newFloorOffset + currentRoom.Length + 1;
            newFloorHeight = newFloorHeight + 1;
        }
        //create target tiles - rotations are dealt with in CreateTile
        else if (currentTileMapCode == Key.TargetN)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        else if (currentTileMapCode == Key.TargetS)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        else if (currentTileMapCode == Key.TargetE)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        else if (currentTileMapCode == Key.TargetW)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        //create lock tile
        else if (currentTileMapCode == Key.Lock)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        //create ramp tiles - rotation is dealt with in CreateTile
        else if (currentTileMapCode == Key.RampN)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        else if (currentTileMapCode == Key.RampS)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        else if (currentTileMapCode == Key.RampE)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        else if (currentTileMapCode == Key.RampW)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateTile(currentTileMapCode, colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        //create key
        else if (currentTileMapCode == Key.Key)
        {
            for (int y = 0; y < WallHeight; y++)
            {
                CreateKey(colNum, y + newFloorHeight, rowNum - newFloorOffset);
            }
        }
        else
        {
            //create floor tile
            Tile tile = CreateTile(currentTileMapCode, colNum, 0 + newFloorHeight, rowNum - newFloorOffset);
            //add floor objects
            AddFloorTileObjects(tile, currentMapCode, currentTileMapCode);
        }

    }

    private void CreateRoomMapLayout()
    {
        currentRoomPortals = new List<Portal>();

        //get the rows of the map layout of the current room and trim of any extra blank lines
        string[] layoutRows = currentRoom.Layout.Split('\n');
        //loop through and read each map code character row by row and column by column
        for (int rowIndex = 0; rowIndex < ((currentRoom.Length * currentRoom.Floors) + (currentRoom.Floors -1)); rowIndex++)
        {
            //trim any tab characters
            string layoutRow = layoutRows[rowIndex].Trim('\t');

            int numTiles = layoutRow.Length;
            for (int i = 0; i < numTiles; i++)
            {
                AddTile(layoutRow[i], rowIndex, i);
            }
        }
    }

    private void BuildRoom(int newRoomNumber)
    {
        newFloorOffset = 0;
        newFloorHeight = 0;
        //set current room
        currentRoom = rooms.Collection[newRoomNumber];
        //deactivate player
        Player.SetActive(false);
        //destroy any tiles existing
        DestroyLayoutTiles();
        //resize room to required
        ResizeRoom();
        //create the map layout
        CreateRoomMapLayout();
        //enter the room (position player ect)
        EnterRoom(RoomNumber);
        //set new room number
        RoomNumber = newRoomNumber;
        //activate player
        Player.SetActive(true);
        //get the weapon script from the weapon
        FPSWeapon weapon = GameObject.FindWithTag("weapon").GetComponent<FPSWeapon>();
        //set new max ammo and set weapon to full ammo
        weapon.MaxAmmo = currentRoom.MaxAmmo;
        weapon.Ammo = currentRoom.MaxAmmo;
    }

    private void CreatePortal(Tile tile, int toRoomNumber)
    {
        GameObject portalGameObject = GameObject.Instantiate(PortalPrefab);
        Portal portal = portalGameObject.GetComponent<Portal>();
        //set portal's position and parent
        portal.transform.position = tile.transform.position;

        portal.transform.parent = parentTileAnchor.transform;

        //set the room the portal leads to
        portal.ToRoom = toRoomNumber;

        //add portal to portal list
        currentRoomPortals.Add(portal);
        //initially deactivate the portal
        portal.gameObject.SetActive(false);
        isPortalActive = false;
        
        
    }

    private void AddFloorTileObjects(Tile tile, char mapChar, Key layoutMapCodeType)
    {
        switch (layoutMapCodeType)
        {
            //add player to passed in tile
            case Key.Player:
                if (isFirstRoom == true)
                {
                    Player.transform.position = tile.transform.position;
                    isFirstRoom = false;
                }
                break;
             //add portal to passed in tile
            case Key.Door:
                CreatePortal(tile, int.Parse(mapChar.ToString()));
                break;
            default:
                break;
        }

    }

    private void OnRoomChanged(int roomNumber)
    {
        //if room is changed, build the room
        BuildRoom(roomNumber);
    }

    private void OnDestroy()
    {
        //remove the event listeners
        Messenger<int>.RemoveListener(GameEvent.ENTERED_PORTAL, OnRoomChanged);
        Messenger.RemoveListener(GameEvent.KEY_COLLECTED,ActivatePortal);
        Messenger.RemoveListener(GameEvent.RESTART_GAME, RestartScene);
        Messenger.RemoveListener(GameEvent.RESTART_LEVEL, RestartLevel);
    }

    private void EnterRoom(int FromRoomNumber)
    {
        foreach (Portal portal in currentRoomPortals)
        {
            if (isFirstRoom||portal.ToRoom == FromRoomNumber)
            {
                //set player's position to portal's position if not in the first room
                portal.JustArrived = true;
                Player.transform.position = portal.transform.position;
                isFirstRoom = false;
            }
        }
    }

    private void CreateKey(int x, int y, int z)
    {
        float tileScaleHalf = 0.5f;
        //initiate key prefab
        GameObject key = GameObject.Instantiate(keyPrefab);
        float room_Tile1_PosX = currentRoom.Width / 2.0f;
        float room_Tile1_PosZ = -currentRoom.Length / 2.0f;

        float xPos = (room_Tile1_PosX - x) - tileScaleHalf;
        float zPos = (room_Tile1_PosZ + z) + tileScaleHalf;
        float yPos = y + 1.0f;
        //position and set parent
        key.transform.position = new Vector3(xPos, yPos, zPos);
        key.transform.parent = parentTileAnchor.transform;
        if (newFloorOffset == 0)
        {
            AddTile('.', z , x);
        }
        
    }

    private void ActivatePortal()
    {
        foreach (Transform child in parentTileAnchor.transform)
        {
            if (child.tag == "portal")
            {
                //find portal object, get it's portal component
                Portal portal = child.GetComponent<Portal>();
                if (portal.ToRoom > currentRoom.Number)
                {
                    //if this is a portal to the next room, activate it, set it's active flag to true
                    child.gameObject.SetActive(true);
                    isPortalActive = true;
                }
                
            }
        }
    }

    private void RestartScene()
    {
        //reload current scene
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.buildIndex);
    }

    private void RestartLevel()
    {
        //if in first room, rebuild room as first room
        if (currentRoom.Number == 0)
        {
            isFirstRoom = true;
            BuildRoom(currentRoom.Number);
        }
        else
        {
            //reactivate portal
            isPortalActive = true;
            //get room number from previous pirtal
            RoomNumber = RoomNumber - 1;
            if(RoomNumber == 0)
            {
                //if previous room was first room
                isFirstRoom = true;
            }
            //build room next from previous room (i.e this room)
            BuildRoom(RoomNumber + 1);
        }
        
    }
}
