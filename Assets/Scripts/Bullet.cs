﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Bullet : MonoBehaviour {

    //bullet speed is public so it can be set to any speed through the Unity Editor, useful for if new bullet types were to be introduced
    public float bulletSpeed;
    //variable to hold camera direction
    private Vector3 fwd;
	// Use this for initialization
	void Start () {
        //set the bullet's name
        SetGameObjectName();
        //place the bullet in the bulletHolder Game Object ( make the bulletHolder the parent)
        this.transform.parent = GameObject.Find("bulletHolder").transform;
        //get the direction the main camera is facing
        fwd = GameObject.FindWithTag("MainCamera").transform.forward;
    }
	
	// Update is called once per frame
	void Update () {
        //move the bullet
        transform.position = (transform.position + (fwd * bulletSpeed));
	}

    private void SetGameObjectName()
    {
        //simply makes each bullet's name "bullet". no need to distinguish, each handles it's own destruction call
        name = string.Format("bullet");
    }

    private void OnTriggerEnter(Collider other)
    {
        //if the bullet collides with anything oother than the player and/or the portal, braodcast the event
        if (other.gameObject.tag.ToLower() != "player" && other.gameObject.tag.ToLower() != "portal")
        {
            Collider bullet = this.GetComponent<Collider>();
            Messenger<Collider>.Broadcast(GameEvent.BULLET_HIT, bullet);
            
        }
    }
}
