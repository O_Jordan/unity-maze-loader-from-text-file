﻿using UnityEngine;

public class Keycontrol : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        //if player collides with the key
        if (other.tag.ToLower() == "player")
        {
            //broadcast the KEY__COLLECTED event
            Messenger.Broadcast(GameEvent.KEY_COLLECTED);
            //deactivate the key, essentially removing it from play
            transform.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        //rotates the key every frame, independent of frame rate
        transform.Rotate(new Vector3(0f, 20f, 0f) * Time.deltaTime);
    }
}
